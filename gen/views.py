from rauth import OAuth2Service
from django.http import HttpResponse, HttpResponseRedirect
import json
import urllib2
import urllib
from sanction.client import Client

import agiliq_params

agiliq = OAuth2Service(
    client_id=agiliq_params.CLIENT_ID,
    client_secret=agiliq_params.CLIENT_SECRET,
    authorize_url=agiliq_params.AUTHORIZE_URL,
    access_token_url=agiliq_params.ACCESS_TOKEN_URL,
    )

agiliq_client = Client(
        auth_endpoint=agiliq_params.AUTHORIZE_URL,
        token_endpoint=agiliq_params.ACCESS_TOKEN_URL,
        client_id=agiliq_params.CLIENT_ID,
        client_secret=agiliq_params.CLIENT_SECRET,
        redirect_uri='http://localhost:8000/authorize_code',
        )

def get_auth_url(request):
    # session = agiliq.get_auth_session()
    # params = {'response_type': 'token',
    #          'redirect_uri': 'http://localhost:8000/authorize_code'}
    # return HttpResponseRedirect(agiliq.get_authorize_url(**params))
    return HttpResponseRedirect(agiliq_client.auth_uri())

def authorize_code(request):
    code = request.GET.get('code')
    #data = {'code': code,
            #'redirect_uri': 'http://localhost:8000/authorize_code',
    #        'grant_type': 'authorization_code'}
    
    #creds = (agiliq_params.CLIENT_ID, agiliq_params.CLIENT_SECRET)

    # s = agiliq.get_auth_session(data=data,
    #                            auth=creds,  # Basic Auth
    #                            )

    # user = s.get('me').json()
    # print 'Currently logged in as {name}'.format(name=user['name'])
    # user = agiliq.get_session(code)
    #kwargs = {'client_id':agiliq_params.CLIENT_ID,'client_secret':agiliq_params.CLIENT_SECRET, 'code':code,}
    #session = agiliq.get_auth_session('GET',**kwargs)
    #return HttpResponse(s)
    #url = "%s?%s" %(agiliq_params.ACCESS_TOKEN_URL, urllib.urlencode(kwargs))
    agiliq_client.request_token({ 'code':code , 'grant_type':'authorization_code',})
    
    return HttpResponse(agiliq_client.access_token)
