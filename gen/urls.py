from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from .views import get_auth_url, authorize_code
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gen.views.home', name='home'),
    # url(r'^gen/', include('gen.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'get_auth_url', get_auth_url),
#    url(r'^authorize_coder?(?P<code>.+)/$', authorize_code)
    url(r'^authorize_code/', authorize_code),
)
