I'm getting a 403 error once it requests the access token.

You have to access '/get_auth_url' from your browser which takes us to the authorization page.

After you authorize however, trying to get the access token by making a POST request is giving a 403 Forbidden Error.

I'm sending all the params specified:
    client_id
    client_secret
    redirect_uri
    code
    grant_type

